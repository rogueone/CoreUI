//
//  ViewController.swift
//  CoreUI
//
//  Created by Thibault Wittemberg on 17-01-08.
//  Copyright © 2017 Starfleet. All rights reserved.
//

import UIKit
import CoreApi

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let city = City(name: "Montréal")
        print ("City is \(city)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

